enum ThemeAction {
	setTheme = 'SET_THEME',
	setPrimaryColor = 'SET_PRIMARY_COLOR'
}

export {
    ThemeAction
};
import { IStoreInitialThemeState } from 'src/context/interfaces/theme/IStoreInitialThemeState';

interface IStoreInitialState {
	theme: IStoreInitialThemeState;
}

export type {
    IStoreInitialState
};